<?php

namespace Database\Seeders;

use App\Models\Category;
use App\Models\Game;
use App\Models\User;
use Illuminate\Database\Seeder;

class GameSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $games = Game::factory(100)->create();
        $user = User::find(1);
        foreach ($games as $game)
        {
            $game->user()->associate($user);
            $game->categories()->attach(rand(1, 5));
        }

    }
}
