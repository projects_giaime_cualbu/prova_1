<?php

use App\Http\Controllers\CategoryController;
use App\Http\Controllers\GameController;
use App\Http\Controllers\PhoneController;
use App\Http\Controllers\ProfileController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route homepage

Route::get('/', function () {
    return view('home');
});

// Route Games

Route::resource('games', GameController::class);

// Route Category

Route::resource('categories', CategoryController::class);

// Route Profile

Route::resource('profiles', ProfileController::class);

Route::get('aggiungi/telefono', [PhoneController::class, 'create'])->name('phones.create');
Route::post('salva/numero', [PhoneController::class, 'store'])->name('phones.store');
Route::get('modifica/numero', [PhoneController::class, 'edit'])->name('phones.edit');


