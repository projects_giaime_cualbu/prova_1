<?php

namespace App\Http\Controllers;

use App\Models\Phone;
use App\Models\Profile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PhoneController extends Controller
{
    public function create()
    {
        return view('profiles.phones.create');
    }

    public function store(Request $request)
    {
        $phone = Auth::user()->phones()->create($request->all());
        return redirect(route('profiles.index'))->with('message', 'Telefono aggiunto con successo');
    }

    public function edit(Phone $phone)
    {
    
        // $phone = Phone::all(['number']);
        // $number = Auth::user()->phones()->get($phone->number);
        return view('profiles.phones.edit', compact('phone'));
    }
}
