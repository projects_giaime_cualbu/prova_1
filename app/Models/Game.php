<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Game extends Model
{
    use HasFactory;

    protected $fillable = ['title', 'description', 'img'];

    //One to many con user

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    //Many to many con categories

    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }
}
