document.addEventListener('scroll', function(){
    const navbar = document.querySelector('#p-navbar')

    if(window.scrollY >150){
        navbar.classList.add('navanimation')

    }
    else {
        navbar.classList.remove('navanimation')
    }
})

document.addEventListener('scroll', function(){
    const titleanim = document.querySelector('.headertitle')

    titleanim.classList.add('active');
})

//glide

var glide = new Glide('glide', {
    type: 'carousel',
    perView: 4,
    focusAt: 'center',
    breakpoints: {
      800: {
        perView: 2
      },
      480: {
        perView: 1
      }
    }
  })
  
  glide.mount()