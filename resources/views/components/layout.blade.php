<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{$title ?? 'Blog qualsiasi'}}</title>
    <link rel="stylesheet" href="/css/app.css">
    {{$style ?? ''}}
</head>
<body>
    <x-navbar />
    
    {{$slot}}

    <x-footer />

    <script src="/js/app.js"></script>
    {{$script ?? ''}}
    <script src="https://kit.fontawesome.com/4d1509abac.js" crossorigin="anonymous"></script>
</body>
</html>