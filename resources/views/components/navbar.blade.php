<nav id="p-navbar" class="navbar navbar-expand-lg navbarcustom fixed-top" >
    <div class="container-fluid">
      <a class="acustom navbar-brand" href="/">BlogDiVideogiochi</a>
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav me-auto mb-2 mb-lg-0">
          <li class="nav-item">
            <a class="acustom nav-link active" aria-current="page" href="{{route('games.index')}}">Tutti i videogiochi</a>
          </li>
          <li class="nav-item">
            <a class="acustom nav-link active" aria-current="page" href="{{route('categories.index')}}">Tutte le categorie</a>
          </li>
          @auth
          <li class="nav-item">
            <a class="acustom nav-link active" href="{{route('games.create')}}">Inserisci videogioco</a>
          </li>
          @endauth
          @guest
          <li class="nav-item">
            <a class="acustom nav-link" href="{{route('login')}}">Login</a>
          </li>
          <li class="nav-item">
            <a class="acustom nav-link" href="{{route('register')}}">Registrati</a>
          </li>
          @else
          <li class="nav-item dropdown">
            <a class="acustom nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
              {{Auth::user()->name}}
            </a>
            <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
              <li><a class="acustom dropdown-item" href="{{route('profiles.index')}}">Profilo</a></li>
              <li><a class="acustom dropdown-item" href="" onclick="event.preventDefault();document.getElementById('form-logout').submit()">Logout</a></li> 
            </ul>
          </li>
          <form action="{{route('logout')}}" method="POST" id="form-logout">
          @csrf
          </form>
          @endguest       
        </ul>
        <form class="d-flex">
          <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
          <button class="btn btn-outline-success" type="submit">Search</button>
        </form>
      </div>
    </div>
  </nav>