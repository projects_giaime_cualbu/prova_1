<x-layout>
    <x-slot name="title">Videogiochi per categoria</x-slot>
    <style>
        
    </style>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 text-center my-5">
                <h1>Ecco i videogiochi della categoria {{$category->name}}!</h1>
            </div>
            @if (session('message'))
                <div class="alert alert-success shadow text-center my-5">
                    <p>{{session('message')}}</p>
                </div>
            @endif
            @if ($errors->any())
                @foreach ($errors->all() as $error)
                <div class="alert alert-danger shadow text-center my-5">
                    <p>{{$error}}</p>
                </div>
                @endforeach
            @endif
            @foreach ($category->games as $game)
            <div class="col-12 col-md-4">
                <div class="card my-3">
                    <img src="{{$game->img ? Storage::url($game->img) : 'https://picsum.photos/200'}}" class="card-img-top img-custom" alt="...">
                    <div class="card-body">
                      <h5 class="card-title">{{$game->title}}</h5>
                      <p class="card-text">Categorie:
                        <ul>
                            @foreach ($game->categories as $category)
                                <li><a href="{{route('categories.show',compact('category'))}}">{{$category->name}}</a></li>
                            @endforeach
                        </ul></p>
                      <p class="card-text">Autore: {{$game->user ? $game->user->name : 'Sconosciuto'}}</p>
                      <p class="card-text">Descrizione: {{$game->description}}</p>
                      <a href="{{route('games.show', compact('game'))}}" class="btn btn-primary">Visualizza dettagli</a>
                    </div>
                  </div>
            </div>
           
            @endforeach
            <div class="d-flex justify-content-center mt-5">
                {{-- {{$category->links()}} --}}
            </div>
        </div>
    </div>
    
    






</x-layout>