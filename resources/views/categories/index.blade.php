<x-layout>
    <x-slot name="title">Categorie</x-slot>
    <style>
        
    </style>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 text-center my-5">
                <h1>Ecco tutti i videogiochi!</h1>
            </div>
            @if (session('message'))
                <div class="alert alert-success shadow text-center my-5">
                    <p>{{session('message')}}</p>
                </div>
            @endif
            @if ($errors->any())
                @foreach ($errors->all() as $error)
                <div class="alert alert-danger shadow text-center my-5">
                    <p>{{$error}}</p>
                </div>
                @endforeach
            @endif
            @foreach ($categories as $category)
            <div class="col-12 col-md-4">
                <div class="card my-3">
                    <img src="https://picsum.photos/200" class="card-img-top img-custom" alt="...">
                    <div class="card-body">
                      <h5 class="card-title mb-3">{{$category->name}}</h5>
                      
                      <a href="{{route('categories.show', compact('category'))}}" class="btn btn-primary">Visualizza dettagli</a>
                      
                    </div>
                  </div>
            </div>
            @endforeach
            <div class="d-flex justify-content-center mt-5">
                {{$categories->links()}}
            </div>
        </div>
    </div>
    
    






</x-layout>