<x-layout>
    
    <x-slot name="title">Aggiungi videogioco</x-slot>

    <div class="container-fluid">
        <div class="row">
            <div class="col-12 my-5 text-center">
                <h1>Aggiungi un videogioco!!</h1>
            </div>
            @if (session('message'))
                <div class="alert alert-success shadow text-center my-5">
                    <p>{{session('message')}}</p>
                </div>
            @endif
            @if ($errors->any())
                @foreach ($errors->all() as $error)
                <div class="alert alert-danger shadow text-center my-5">
                    <p>{{$error}}</p>
                </div>
                @endforeach
            @endif
            <div class="col-12">
                <form action="{{route('games.store')}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="mb-3">
                      <label for="nameGame1" class="form-label">Nome Videogioco</label>
                      <input name="title" type="text" class="form-control" id="nameGame1">
                    </div>
                    <div class="mb-3">
                        <label for="description1" class="form-label">Descrizione videogioco</label>
                        <input name="description" type="text" class="form-control" id="description1">
                    </div>
                    <div class="mb-3">
                        <input name="img" type="file" class="form-control" placeholder="Inserisci un immagine">
                    </div>
                    <select name="categories[]" class="form-select" multiple aria-label="multiple select example">
                        <option selected>Scegli la categoria</option>
                        @foreach ($categories as $category)
                        <option value="{{$category->id}}">{{$category->name}}</option>
                            
                        @endforeach
                        
                      </select>
                    
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </form>
            </div>
        </div>
    </div>







</x-layout>