<x-layout>
    
    <x-slot name="title">Modifica videogioco</x-slot>

    <div class="container-fluid">
        <div class="row">
            <div class="col-12 my-5 text-center">
                <h1>Modifica videogioco!!</h1>
            </div>
            @if (session('message'))
                <div class="alert alert-success shadow text-center my-5">
                    <p>{{session('message')}}</p>
                </div>
            @endif
            @if ($errors->any())
                @foreach ($errors->all() as $error)
                <div class="alert alert-danger shadow text-center my-5">
                    <p>{{$error}}</p>
                </div>
                @endforeach
            @endif
            <div class="col-12">
                <form action="{{route('games.update', compact('game'))}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    <div class="mb-3">
                      <label for="nameGame1" class="form-label">Nome Videogioco</label>
                      <input name="title" type="text" class="form-control" id="nameGame1" value="{{$game->title}}">
                    </div>
                    <div class="mb-3">
                        <label for="description1" class="form-label">Descrizione videogioco</label>
                        <input name="description" type="text" class="form-control" id="description1" value="{{$game->description}}">
                    </div>
                    <div class="mb-3">
                        <input name="img" type="file" class="form-control" placeholder="Inserisci un immagine" value="{{$game->img}}">
                    </div>
                    
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </form>
            </div>
        </div>
    </div>







</x-layout>