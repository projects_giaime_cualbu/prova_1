<x-layout>
    <x-slot name="title">Videogiochi</x-slot>
    <style>
        
    </style>
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 text-center my-5">
                <h1>Ecco tutti i videogiochi!</h1>
            </div>
            @if (session('message'))
                <div class="alert alert-success shadow text-center my-5">
                    <p>{{session('message')}}</p>
                </div>
            @endif
            @if ($errors->any())
                @foreach ($errors->all() as $error)
                <div class="alert alert-danger shadow text-center my-5">
                    <p>{{$error}}</p>
                </div>
                @endforeach
            @endif
            @foreach ($games as $game)
            <div class="col-12 col-md-4">
                <div class="card my-3">
                    <img src="{{$game->img ? Storage::url($game->img) : 'https://picsum.photos/200'}}" class="card-img-top img-custom" alt="...">
                    <div class="card-body">
                      <h5 class="card-title">{{$game->title}}</h5>
                      <p class="card-text">Categorie:
                        <ul>
                            @foreach ($game->categories as $category)
                                <li><a href="{{route('categories.show',compact('category'))}}">{{$category->name}}</a></li>
                            @endforeach
                        </ul></p>
                      <p class="card-text">Autore: {{$game->user ? $game->user->name : 'Sconosciuto'}}</p>
                      <p class="card-text">Descrizione: {{$game->description}}</p>
                      <a href="{{route('games.show', compact('game'))}}" class="btn btn-primary">Visualizza dettagli</a>
                      <a href="{{route('games.edit', compact('game'))}}" class="btn btn-warning"><i class="fas fa-edit"></i></a>
                      <a href="" class="btn btn-danger" data-bs-toggle="modal" data-bs-target="#modal{{$game->id}}"><i class="fas fa-trash"></i></a>
                    </div>
                  </div>
            </div>
            <div class="modal" tabindex="-1" id="modal{{$game->id}}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Modal title {{$game->id}}</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <p>Modal body text goes here.</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <form action="{{route('games.destroy',compact('game'))}}" method="POST">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger my-4">Elimina</button>
                        </form>
                    </div>
                    </div>
                </div>
            </div>
            @endforeach
            <div class="d-flex justify-content-center mt-5">
                {{$games->links()}}
            </div>
        </div>
    </div>
    
    






</x-layout>