<x-layout> 
    <x-slot name="title">Modifica numero</x-slot>

    <h1 class="text-center my-5">Modifica numero di telefono</h1>

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-8 text-center">
                <form action="" method="post">
                    @csrf
                    <input name="number" type="text" class="form-control" placeholder="Modifica numero di telefono" value="{{$phone->number}}">
            
                    <button type="submit" class="btn btn-primary my-5 ">Salva</button>
                </form>
            </div>
        </div>
    </div>






</x-layout>