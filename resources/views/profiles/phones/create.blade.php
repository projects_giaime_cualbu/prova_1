<x-layout> 
    <x-slot name="title">Aggiungi numero</x-slot>

    <h1 class="text-center my-5">Aggiungi numero di telefono</h1>

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-8 text-center">
                <form action="{{route('phones.store')}}" method="post">
                    @csrf
                    <input name="number" type="text" class="form-control" placeholder="Inserisci numero di telefono">
            
                    <button type="submit" class="btn btn-primary my-5 ">Salva</button>
                </form>
            </div>
        </div>
    </div>






</x-layout>