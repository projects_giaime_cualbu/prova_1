<x-layout>
    <x-slot name="title">Profilo</x-slot>

    <div class="my-5 d-flex justify-content-center">
        <h1 class="fw-bold">Il Mio profilo Personale</h1>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-4">
                <h2 class="text-center">I tuoi dati personali</h2>
                <div class="ms-3">
                <p>Nome: {{$user->name}}</p>
                <p>Email: {{$user->email}}</p>
                <p>Telefono:
                    @if ($user->phones)
                    {{$user->phones->number}}
                    <a href="{{route('phones.edit')}}" class="btn btn-warning shadow ms-5">Modifica numero di telefono</a>

                    
                    @else

                    <a href="{{route('phones.create')}}" class="btn btn-primary">Aggiungi numero di telefono</a>

                    @endif
                    
                    
                    
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <h1 class="text-center">I tuoi Giochi</h1>
            @foreach ($userGames as $game)
            <div class="col-12 col-md-4 d-flex justify-content-center">
                <div class="card my-3" style="width: 25rem">
                    <img src="{{$game->img ? Storage::url($game->img) : 'https://picsum.photos/200'}}" class="card-img-top img-custom" alt="...">
                    <div class="card-body">
                      <h5 class="card-title">{{$game->title}}</h5>
                      <p class="card-text">Categorie:
                        <ul>
                            @foreach ($game->categories as $category)
                                <li><a href="{{route('categories.show',compact('category'))}}">{{$category->name}}</a></li>
                            @endforeach
                        </ul></p>
                      <p class="card-text">Autore: {{$game->user ? $game->user->name : 'Sconosciuto'}}</p>
                      <p class="card-text">Descrizione: {{$game->description}}</p>
                      <a href="{{route('games.show', compact('game'))}}" class="btn btn-primary">Visualizza dettagli</a>
                      <a href="{{route('games.edit', compact('game'))}}" class="btn btn-warning"><i class="fas fa-edit"></i></a>
                      <a href="" class="btn btn-danger" data-bs-toggle="modal" data-bs-target="#modal{{$game->id}}"><i class="fas fa-trash"></i></a>
                    </div>
                  </div>
            </div>
            <div class="modal" tabindex="-1" id="modal{{$game->id}}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Modal title {{$game->id}}</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <p>Modal body text goes here.</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <form action="{{route('games.destroy',compact('game'))}}" method="POST">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger my-4">Elimina</button>
                        </form>
                    </div>
                    </div>
                </div>
            </div>
            @endforeach
            
        </div>
    </div>









</x-layout>