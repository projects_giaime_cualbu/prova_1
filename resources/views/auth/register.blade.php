<x-layout>

  <x-slot name="title">Registrati</x-slot>


        <div class="container-fluid">
            <div class="row">
                <div class="col-8 my-5">
                 <form action="{{route('register')}}" method="POST">
                     @csrf
                     <div class="mb-3">
                        <label for="textInput1" class="form-label">Name</label>
                        <input name="name" type="text" class="form-control" id="textInput1" value="{{old('name')}}">
                      </div>
                     <div class="mb-3">
                       <label for="exampleInputEmail1" class="form-label">Email address</label>
                       <input name="email" type="email" class="form-control" id="exampleInputEmail1" value="{{old('email')}}">
                     </div>
                     <div class="mb-3">
                       <label for="exampleInputPassword1" class="form-label">Password</label>
                       <input name="password" type="password" class="form-control" id="exampleInputPassword1" value="{{old('password')}}">
                     </div>
                     <div class="mb-3">
                        <label for="exampleInputPassword1" class="form-label">ConfirmPassword</label>
                        <input name="password_confirmation" type="password" class="form-control" id="exampleInputPassword1" value="{{old('password_confirmation')}}">
                      </div>
                     <button type="submit" class="btn btn-primary">Submit</button>
                   </form>
                   @if ($errors->any())
                     @foreach ($errors->all() as $error)
                         <div class="alert alert-danger shadow my-5">
                             <p>{{$error}}</p>
                         </div>
                     @endforeach     
                   @endif
                </div>
            </div>
        </div>
     
     
     
     
     



</x-layout>